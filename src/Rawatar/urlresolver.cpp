/*
 *    Copyright (c) 2013 Sune Vuorela <sune@vuorela.dk>
 *
 *    Permission is hereby granted, free of charge, to any person
 *    obtaining a copy of this software and associated documentation
 *    files (the "Software"), to deal in the Software without
 *    restriction, including without limitation the rights to use,
 *    copy, modify, merge, publish, distribute, sublicense, and/or sell
 *    copies of the Software, and to permit persons to whom the
 *    Software is furnished to do so, subject to the following
 *    conditions:
 *
 *    The above copyright notice and this permission notice shall be
 *    included in all copies or substantial portions of the Software.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *    OTHER DEALINGS IN THE SOFTWARE
 */


#include "urlresolver.h"
#include "urlresolver_p.h"

#include <QDnsLookup>
#include <QUrl>
#include <QStringList>
#include <qurlquery.h>
#include <qcryptographichash.h>

Rawatar::UrlResolverPrivate::UrlResolverPrivate(UrlResolver* parent) : QObject(parent),
    q(parent), size(80), fallBackToGravatar(false), useHttps(false) {
}

Rawatar::UrlResolver::UrlResolver(QObject* parent): QObject(parent), d(new UrlResolverPrivate(this)) {

}

Rawatar::UrlResolver::~UrlResolver() {

}

void Rawatar::UrlResolver::start() {
    QString domain = d->address.split('@').last();
    if(domain.isEmpty()) {
        emit urlResolved(QUrl());
        return;
    }
    if(d->useHttps) {
        domain.prepend("_avatars-sec._tcp.");
    } else {
        domain.prepend("_avatars._tcp.");
    }
    QDnsLookup* dnslookup = new QDnsLookup(this);
    dnslookup->setType(QDnsLookup::SRV);
    dnslookup->setName(domain);
    connect(dnslookup,SIGNAL(finished()),d,SLOT(resolverFinished()));
    dnslookup->lookup();
}

QString Rawatar::UrlResolverPrivate::calculateHash() {
    QCryptographicHash hash(fallBackToGravatar ? QCryptographicHash::Md5 : QCryptographicHash::Sha256);
    hash.addData(address.toLower().toUtf8());
    return QString::fromUtf8(hash.result().toHex());
}

void Rawatar::UrlResolverPrivate::resolverFinished() {
    QObject* senderObject = sender();
    QDnsLookup* dnslookup = qobject_cast<QDnsLookup*>(senderObject);
    if(!dnslookup) {
        // erm. something connected here in a wrong way.
        emit q->urlResolved(QUrl());
        return;
    }
    QList<QDnsServiceRecord> serviceRecords = dnslookup->serviceRecords();
    QString hostname;
    int port;
    if(serviceRecords.isEmpty()) {
        hostname = useHttps ? "seccdn.libravatar.org" : "cdn.libravatar.org";
        port = useHttps ? 443 : 80;
    } else {
        hostname = serviceRecords.first().target();
        port = serviceRecords.first().port();
    }
    QUrlQuery query;
    if(!defaultAvatar.isEmpty()) {
        query.addQueryItem("d",defaultAvatar);
    }
    query.addQueryItem("s",QString::number(size));
    QUrl avatarurl;
    avatarurl.setScheme(useHttps ? "https" : "http");
    avatarurl.setHost(hostname);
    avatarurl.setPort(port);
    avatarurl.setPath("/avatar/"+calculateHash());
    avatarurl.setQuery(query);
    emit q->urlResolved(avatarurl);
}

QString Rawatar::UrlResolver::address() const {
    return d->address;
}

QString Rawatar::UrlResolver::defaultAvatar() const {
    return d->defaultAvatar;
}

bool Rawatar::UrlResolver::fallBackToGravatar() const {
    return d->fallBackToGravatar;
}

void Rawatar::UrlResolver::setAddress ( const QString& newaddress ) {
    d->address = newaddress;
}

void Rawatar::UrlResolver::setDefaultAvatar ( const QString& defaultAvatar ) {
    d->defaultAvatar = defaultAvatar;
}

void Rawatar::UrlResolver::setFallBackToGravatar ( bool enable ) {
    d->fallBackToGravatar = enable;
}

void Rawatar::UrlResolver::setSize ( int newsize ) {
    d->size = newsize;
}

int Rawatar::UrlResolver::size() const {
    return d->size;
}

void Rawatar::UrlResolver::setUseHttps ( bool useHttps ) {
    d->useHttps = useHttps;
}

bool Rawatar::UrlResolver::useHttps() const {
    return d->useHttps;
}
